package com.ufo_alert.ufo_alert;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.widget.Button;

import com.ufo_alert.ufo_alert.Fragments.FragmentLogIn;
import com.ufo_alert.ufo_alert.Fragments.FragmentPrincipal;
import com.ufo_alert.ufo_alert.Fragments.FragmentSignUp;

public class MainActivity extends AppCompatActivity implements FragmentPrincipal.Callback {
    Button name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.fragmentprincipal, new FragmentPrincipal());
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onButtonPressed(int id) {

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        switch (id){
            case 1:
                break;
            case 2:
                ft.add(R.id.fragmentprincipal, new FragmentLogIn());
                break;
            case 3:
                ft.add(R.id.fragmentprincipal, new FragmentSignUp());
                break;
        }
        ft.addToBackStack("principal");
        ft.commit();

    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount() == 1){

            finish();
        }else {
            super.onBackPressed();
        }
    }
}
