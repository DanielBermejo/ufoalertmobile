package com.ufo_alert.ufo_alert.Fragments;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.ufo_alert.ufo_alert.R;

import org.json.JSONArray;

public class FragmentLogIn extends Fragment implements View.OnClickListener, Response.ErrorListener, Response.Listener<JSONArray>  {

    EditText emaillog, passlog;

    Button login;
    String baseUrl = "http://192.168.0.169:8006/requestandroid";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v   = inflater.inflate(R.layout.fragment_login, container, false);

        emaillog = v.findViewById(R.id.emaillog);
        passlog  = v.findViewById(R.id.passwordlog);

        login    = v.findViewById(R.id.login);
        login.setOnClickListener(this);

        return v;

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.login){

            if(emaillog.getText().toString().isEmpty() || passlog.getText().toString().isEmpty()){

                Toast.makeText(getContext(),R.string.missingdata,Toast.LENGTH_SHORT).show();
            }
            else{
                /*
                final List<NameValuePair> postParameters = new ArrayList<NameValuePair>(4);
                postParameters.add(new BasicNameValuePair("name", namesign.getText().toString()));
                postParameters.add(new BasicNameValuePair("email", emaillog.getText().toString()));
                postParameters.add(new BasicNameValuePair("password", passlog.getText().toString()));
                postParameters.add(new BasicNameValuePair("password_confirmation", confirmpass.getText().toString()));
                 */

                RequestQueue requestQueue= Volley.newRequestQueue(getContext());
                JsonArrayRequest jsonObjectRequest=new JsonArrayRequest(Request.Method.POST, baseUrl, null, this, this);
                requestQueue.add(jsonObjectRequest);
            }
        }

    }


    @Override
    public void onErrorResponse(VolleyError error) {
        Log.v("Respuesta", "error: "+error);
    }

    @Override
    public void onResponse(JSONArray response) {
        Toast.makeText(getContext(), response.toString(), Toast.LENGTH_SHORT);

        Log.v("Respuesta", "funciona");

    }
}
