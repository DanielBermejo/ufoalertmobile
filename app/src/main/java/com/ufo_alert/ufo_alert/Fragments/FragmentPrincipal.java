package com.ufo_alert.ufo_alert.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.ufo_alert.ufo_alert.R;

public class FragmentPrincipal extends Fragment implements View.OnClickListener {
    Button signprincipal,logprincipal,reportprincipal;
    Callback callback;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_principal,container,false);
        signprincipal=v.findViewById(R.id.signprincipal);
        logprincipal=v.findViewById(R.id.logprincipal);
        reportprincipal=v.findViewById(R.id.reportprincipal);
        callback=(Callback)getContext();

        signprincipal.setOnClickListener(this);
        logprincipal.setOnClickListener(this);
        reportprincipal.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
            switch (v.getId()) {
                case R.id.signprincipal:
                    callback.onButtonPressed(3);
                    break;
                case R.id.logprincipal:
                    callback.onButtonPressed(2);
                    break;
                case R.id.reportprincipal:
                    callback.onButtonPressed(1);
                    break;
            }

    }

    public interface Callback{
        void onButtonPressed(int id);
    }

}
